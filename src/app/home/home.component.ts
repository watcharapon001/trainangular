import { Component, OnInit } from '@angular/core';
import { Home, HomeService } from './home.service';
import { Homes } from '../mock-home';
import { observable, of } from 'rxjs';
import { MessageService } from '../message/message.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  homess: Home[] = [];

  title = 'Softsquare';
  homes = Homes;

  home: Home = {
    id : 1,
    name : 'somsak'
  }

  constructor(private homeService: HomeService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getHomes();
  }

  selectedHome?: Home;
  onSelect(home: Home): void {
    this.selectedHome = home;
    this.messageService.add('HomesComponent: Select home id=${home.id}');
  }

  // getHomes(): Home[]{
  //   return Homes;
  // }

  getHomes(): void{
    this.homeService.getHomes().subscribe(homes => this.homes = homes);

  }

}
