import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HomeTownService } from '../home-town.service';
import { Homes } from '../mock-home';
import { MessageService } from '../message/message.service';

export interface Home{
  id: number;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private messageService: MessageService) { }

  getHomes(): Observable<Home[]> {
    const homes = of(Homes);
    this.messageService.add('HomeService: fetched homes');
    return homes;
  }
}
