import { Component, OnInit, Input } from '@angular/core';
import { Home } from '../home/home.service';
import { HomeTownService } from '../home-town.service';

@Component({
  selector: 'app-home-detail',
  templateUrl: './home-detail.component.html',
  styleUrls: ['./home-detail.component.css']
})

export class HomeDetailComponent implements OnInit {

  @Input() home?: Home;

  constructor() { }

  ngOnInit(): void {
  }

}
